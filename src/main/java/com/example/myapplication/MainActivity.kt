package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    @SuppressLint("SetTextI18n")
    private fun init() {
        val randomNumberTextView = findViewById<TextView>(R.id.randomNumber)
        val generateRandomButton = findViewById<Button>(R.id.generateRandomButton)
        val Yes_No = findViewById<TextView>(R.id.Yes_No)
        generateRandomButton.setOnClickListener {
            val number: Int = randomNumber()
            d("ButtonClicked", "$number")
            randomNumberTextView.text = number.toString()
            if (number > 0 && number % 5 == 0) {
                Yes_No.text = "Yes"
            } else {
                Yes_No.text = "No"
            }
        }

    }
    private fun randomNumber(): Int {
        val randomNumber: Int = (-100..100).random()
        return randomNumber
    }
}

